#include <Time.h>
#include <TimeLib.h>
#include <LiquidCrystal_I2C.h>
#include "alarma.h"

#include <DS1307RTC.h>
// Date and time functions using a DS1307 RTC connected via I2C and Wire lib
#include <Wire.h>
#include "RTClib.h"

/* Play Melody
 * -----------
 *
 * Program to play a simple melody
 *
 * Tones are created by quickly pulsing a speaker on and off
 *   using PWM, to create signature frequencies.
 *
 * Each note has a frequency, created by varying the period of
 *  vibration, measured in microseconds. We'll use pulse-width
 *  modulation (PWM) to create that vibration.

 * We calculate the pulse-width to be half the period; we pulse
 *  the speaker HIGH for 'pulse-width' microseconds, then LOW
 *  for 'pulse-width' microseconds.
 *  This pulsing creates a vibration of the desired frequency.
 *
 * (cleft) 2005 D. Cuartielles for K3
 * Refactoring and comments 2006 clay.shirky@nyu.edu
 * See NOTES in comments at end for possible improvements
 */
RTC_DS1307 rellotge_temps_real;


// TONES  ==========================================
// Start by defining the relationship between
//       note, period, &  frequency.
#define  c3    7634
#define  d3    6803
#define  e3    6061
#define  f3    5714
#define  g3    5102
#define  a3    4545
#define  b3    4049
#define  c4    3816    // 261 Hz 
#define  d4    3401    // 294 Hz 
#define  e4    3030    // 329 Hz 
#define  f4    2865    // 349 Hz 
#define  g4    2551    // 392 Hz 
#define  a4    2272    // 440 Hz 
#define  a4s   2146
#define  b4    2028    // 493 Hz 
#define  c5    1912    // 523 Hz
#define  d5    1706
#define  d5s   1608
#define  e5    1517    // 659 Hz
#define  f5    1433    // 698 Hz
#define  g5    1276
#define  a5    1136
#define  a5s   1073
#define  b5    1012
#define c6 955
// Define a special note, 'R', to represent a rest
#define  R     0

// DURATION OF THE NOTES  TODO!!!
#define BPM 120    //  you can change this value changing all the others
#define H 2*Q //half 2/4
#define Q 6000/BPM //quarter 1/4 
#define E Q/2   //eighth 1/8
#define S Q/4 // sixteenth 1/16
#define W 4*Q // whole 4/4

// SETUP ============================================
// Set up speaker on a PWM pin (digital 9, 10 or 11)
int speakerOut = 10;

const int echoPin = 3;
const int trigPin = 5;
const int led = 13;
LiquidCrystal_I2C lcd(0x27, 16, 2);
String answer;


// Do we want debugging on serial out? 1 for yes, 0 for no
int DEBUG = 1;

void setup() {
  pinMode(speakerOut, OUTPUT);
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  pinMode(led, OUTPUT);
  lcd.init();
  lcd.init();
  lcd.backlight();

  
  if (ajustaHora = 1){
    rellotge_temps_real.adjust(DateTime(__DATE__, __TIME__));
  }
  if (DEBUG) {
    Serial.begin(115200); // Set serial out if we want debugging
  }


//TODO !!! store the in PROGMEM to allow change

//  Serial.println("Escriu l'hora de l'alarma així: '9:30', i apreta enter si vols configurar l'alarma...");
//    
//  int WaitStart = millis();
//  while(!Serial.available()) {
//    if(millis() - WaitStart > 10000) break;
//  }
//  while(Serial.available()) {
//    Serial.setTimeout(10000); 
//    alarma= Serial.readString();// read the incoming data as string
//    Serial.println("Alarma configurada a les: " + alarma);
//  }  

}


// MELODY and TIMING  =======================================
//  melody[] is an array of notes, accompanied by beats[],
//  which sets each note's relative length (higher #, longer note)

// Tema Marxa imperial
//int melody[] = { a4, a4,  a4, f4, c5, a4, f4, c5, a4,      e5, e5, e5, f5, c5, g5, f5, c5, a4};
//int beats[] = { 50,  50,  50, 35, 15, 50, 35, 15, 65,     50, 50, 50, 35, 15, 50, 35, 15, 65};        

// Tema star wars
int melody[] = {  f4,  f4, f4,  a4s,   f5,  d5s,  d5,  c5, a5s, f5, d5s,  d5,  c5, a5s, f5, d5s, d5, d5s,   c5};
int beats[] = { 21, 21, 21, 128, 128, 21, 21, 21, 128, 64, 21, 21, 21, 128, 64, 21, 21, 21, 128 }; 

int MAX_COUNT = sizeof(melody) / 2; // Melody length, for looping.

// Set overall tempo
long tempo = 10000;
// Set length of pause between notes
int pause = 1000;
// Loop variable to increase Rest length
int rest_count = 100; //<-BLETCHEROUS HACK; See NOTES

// Initialize core variables
int tone_ = 0;
int beat = 0;
long duration  = 0;
int semaphor = 0; // Posar a 1 si es vol provar melodia independentment de l'alarma

// PLAY TONE  ==============================================
// Pulse the speaker to play a tone for a particular duration
void playTone() {
  
  long elapsed_time = 0;

  if (tone_ > 0) { // if this isn't a Rest beat, while the tone has
    //  played less long than 'duration', pulse speaker HIGH and LOW
    while (elapsed_time < duration) {

      digitalWrite(speakerOut,HIGH);
      delayMicroseconds(tone_ / 2);

      // DOWN
      digitalWrite(speakerOut, LOW);
      delayMicroseconds(tone_ / 2);

      // Keep track of how long we pulsed
      elapsed_time += (tone_);
    }
  }
  else { // Rest beat; loop times delay
    for (int j = 0; j < rest_count; j++) { // See NOTE on rest_count
      delayMicroseconds(duration);  
    }                                
  }                                
}

// LET THE WILD RUMPUS BEGIN =============================
void loop() {
  String hora = getTime();
  
  
  lcd.setCursor(3,0);
  lcd.print(hora);
  Serial.print(hora);
   
  // Set up a counter to pull from melody[] and beats[]
  for (int i=0; i<MAX_COUNT; i++) {
    tone_ = melody[i];
    beat = beats[i];

    duration = beat * tempo; // Set up timing

    
   
   int cm = ping(trigPin, echoPin);
   Serial.print("Distancia: ");
   Serial.println(cm);
   
    String hora = getTime();
    if (alarma+":0" == hora.substring(0, 7)) {
        semaphor = 1;
    } else {
        text(alarma, hora);
    }
       
   if (cm > 30 and semaphor == 1){
     playTone();
   } else {
     tone_ = 0;
     semaphor = 0;
   }
       
    // A pause between notes...
    delayMicroseconds(pause);

    if (DEBUG) { // If debugging, report loop, tone, beat, and duration
      Serial.print(i);
      Serial.print(":");
      Serial.print(beat);
      Serial.print(" ");    
      Serial.print(tone_);
      Serial.print(" ");
      Serial.println(duration);
    }
  }
}

int ping(int trigPin, int echoPin) {
   long duration, distanceCm;
   
   digitalWrite(trigPin, LOW);  //para generar un pulso limpio ponemos a LOW 4us
   delayMicroseconds(4);
   digitalWrite(trigPin, HIGH);  //generamos Trigger (disparo) de 10us
   delayMicroseconds(10);
   digitalWrite(trigPin, LOW);
   
   duration = pulseIn(echoPin, HIGH);  //medimos el tiempo entre pulsos, en microsegundos
   
   distanceCm = duration * 10 / 292/ 2;   //convertimos a distancia, en cm
   return distanceCm;
}


void text(String alarma_text, String hora_text) {
    Serial.print("alarma : ");
    Serial.println(alarma_text);
    Serial.print("hora :  ");
    Serial.println(hora_text);
}

String getTime(){
  tmElements_t temps;
  if (RTC.read(temps)){
    return String(temps.Hour)+':'+String(temps.Minute)+':'+String(temps.Second);
  } else {
    while(0){
      if (RTC.chipPresent()) {
        Serial.println("The DS1307 is stopped.  Please run the SetTime");
        Serial.println("example to initialize the time and begin running.");
        Serial.println();
      } else {
        Serial.println("DS1307 read error!  Please check the circuitry.");
        Serial.println();
      }
      delay(1000);    
    }
  }
}
/*
 * 
 * Notes of todo:
 * - Afegir Temps d'alarma que actualment està a Alarma.h a PROGMEM per poder-se canviar.
 * - Afegir botons per establir alarma
 * 
 * 
 * NOTES of melody
 * The program purports to hold a tone for 'duration' microseconds.
 *  Lies lies lies! It holds for at least 'duration' microseconds, _plus_
 *  any overhead created by incremeting elapsed_time (could be in excess of
 *  3K microseconds) _plus_ overhead of looping and two digitalWrites()
 *  
 * As a result, a tone of 'duration' plays much more slowly than a rest
 *  of 'duration.' rest_count creates a loop variable to bring 'rest' beats
 *  in line with 'tone' beats of the same length.
 *
 * rest_count will be affected by chip architecture and speed, as well as
 *  overhead from any program mods. Past behavior is no guarantee of future
 *  performance. Your mileage may vary. Light fuse and get away.
 *  
 * This could use a number of enhancements:
 * ADD code to let the programmer specify how many times the melody should
 *     loop before stopping
 * ADD another octave
 * MOVE tempo, pause, and rest_count to #define statements
 * RE-WRITE to include volume, using analogWrite, as with the second program at
 *          http://www.arduino.cc/en/Tutorial/PlayMelody
 * ADD code to make the tempo settable by pot or other input device
 * ADD code to take tempo or volume settable by serial communication
 *          (Requires 0005 or higher.)
 * ADD code to create a tone offset (higer or lower) through pot etc
 */
